#!/bin/sh

# update ramdisk from http://get.cm/?device=hlte latest nightly
# by arter97
#
# dependencies : p7zip, unpackbootimg, cpio

rm -rf tmp
mkdir tmp
cd tmp
export date=$(($(date +'%Y%m%d') + 2))
wget http://get.cm/?device=hlte || exit 1
until cat index.html\?device\=hlte | grep $date | tr '\"' '\n' | grep jenkins | grep -v http ; do
	export date=$(($date - 1))
done
rm -rf /tmp/arter97_update_ramdisk
mkdir /tmp/arter97_update_ramdisk

until wget -P /tmp/arter97_update_ramdisk/ http://get.cm$(cat index.html\?device\=hlte | grep $date | tr '\"' '\n' | grep jenkins | grep -v http); do
	rm -rf /tmp/arter97_update_ramdisk/*.zip
done

7z x /tmp/arter97_update_ramdisk/*.zip boot.img
unpackbootimg -i boot.img
mkdir ramdisk
cd ramdisk
gunzip -c ../boot.img-ramdisk.gz | cpio -i

echo "
service after-boot /system/bin/sh /sbin/after-boot.sh
    class late_start
    user root
    oneshot" >> init.qcom.rc

echo "
service dmb /system/bin/dmbserver
    class main
    user root
    oneshot" >> init.carrier.rc

sed -i "/service mpdecision \/system\/bin\/mpdecision --avg_comp/,/disabled/d" init.qcom.rc init.target.rc
sed -i "/service macloader \/system\/bin\/macloader/,/oneshot/d" init.qcom.rc init.target.rc
sed -i -e 's/on fs/    setenforce 0\n    setprop ro.boot.selinux 0\n\non fs/g' init.target.rc

echo "--- a/ramdisk/init.qcom.rc
+++ b/ramdisk/init.qcom.rc
@@ -768,23 +768,25 @@ service profiler_daemon /system/bin/profiler_daemon
     disabled
 
 # virtual sdcard daemon running as system (1000)
-service container /system/bin/sdcard /data/container /mnt/shell/container 1000 1000
+service container /res/busybox mount -t sdcardfs -o rw,nosuid,nodev,noatime,nodiratime,uid=1000,gid=1000,wgid=1015 /data/container /mnt/shell/container
     class late_start
     oneshot
 
     start container
 
 # virtual sdcard daemon running as media_rw (1023)
-service sdcard /system/bin/sdcard -u 1023 -g 1023 -l /data/media /mnt/shell/emulated
+service sdcard /res/busybox mount -t sdcardfs -o rw,nosuid,nodev,noatime,nodiratime,uid=1023,gid=1023,wgid=1015,derive=legacy,reserved_mb=20 /data/media /mnt/shell/emulated
     class late_start
     oneshot
 
-service fuse_sdcard1 /system/bin/sdcard -u 1023 -g 1023 -d /mnt/media_rw/sdcard1 /storage/sdcard1
+service fuse_sdcard1 /res/busybox mount -t sdcardfs -o rw,nosuid,nodev,noatime,nodiratime,uid=1023,gid=1023,wgid=1023,derive=unified,lower_fs=fat /mnt/media_rw/sdcard1 /storage/sdcard1
     class late_start
+    oneshot
     disabled
 
-service fuse_usbdisk /system/bin/sdcard -u 1023 -g 1023 /mnt/media_rw/usbdisk /storage/usbdisk
+service fuse_usbdisk /res/busybox mount -t sdcardfs -o rw,nosuid,nodev,noatime,nodiratime,uid=1023,gid=1023,wgid=1015,derive=unified,lower_fs=fat /mnt/media_rw/usbdisk /storage/usbdisk
     class late_start
+    oneshot
     disabled
 " | patch -p2

echo '--- a/ramdisk/init.carrier.rc
+++ b/ramdisk/init.carrier.rc
@@ -6,30 +6,21 @@
 
 on boot
 # permission for Input Device(Touchkey).
-    chmod 0660 /sys/class/input/input2/enabled
     chown system system /sys/class/input/input2/enabled
+    chmod 0440 /sys/class/input/input2/enabled
     chown radio system /sys/class/sec/sec_touchkey/touch_sensitivity
     chown radio system /sys/class/sec/sec_touchkey/touchkey_firm_update
     chown system radio /sys/class/sec/sec_touchkey/glove_mode
     chown system radio /sys/class/sec/sec_touchkey/flip_mode
     chown system radio /sys/class/sec/sec_touchkey/boost_level
 
-# permission for Input Device(Wacom).
-    chmod 0660 /sys/class/input/input4/enabled
-    chown system system /sys/class/input/input4/enabled
-
-# permissions for S-Pen
-    chmod 0660 /sys/class/input/input3/enabled
-    chown system system /sys/class/input/input3/enabled
-    chown system radio /sys/class/sec/sec_epen/epen_firm_update
-    chown system radio /sys/class/sec/sec_epen/epen_rotation
-    chown system radio /sys/class/sec/sec_epen/epen_hand
-    chown system radio /sys/class/sec/sec_epen/epen_reset
-    chown system radio /sys/class/sec/sec_epen/epen_reset_result
-    chown system radio /sys/class/sec/sec_epen/epen_checksum
-    chown system radio /sys/class/sec/sec_epen/epen_checksum_result
-    chown system radio /sys/class/sec/sec_epen/epen_saving_mode
-    chown system radio /sys/class/sec/sec_epen/boost_level
+# permission for sec_dmb_key
+    chown system system /sys/class/input/input4/enabled
+    chmod 0440 /sys/class/input/input4/enabled
+
+# permission for gpio-keys
+    chown system system /sys/class/input/input18/enabled
+    chmod 0660 /sys/class/input/input18/enabled
 
 # permissions for bluetooth.
     setprop ro.bt.bdaddr_path "/efs/bluetooth/bt_addr"' | patch -p2

echo '--- a/ramdisk/fstab.qcom
+++ b/ramdisk/fstab.qcom
@@ -9,8 +9,6 @@
 /dev/block/platform/msm_sdcc.1/by-name/boot           /boot            emmc    defaults   defaults
 /dev/block/platform/msm_sdcc.1/by-name/recovery       /recovery        emmc    defaults   defaults
 /dev/block/platform/msm_sdcc.1/by-name/system         /system          ext4    ro,errors=panic                                wait
-/dev/block/platform/msm_sdcc.1/by-name/userdata       /data            ext4    nosuid,nodev,noatime,noauto_da_alloc,discard,journal_async_commit,errors=panic      wait,check,encryptable=footer
-/dev/block/platform/msm_sdcc.1/by-name/cache          /cache           ext4    nosuid,nodev,barrier=1 wait,check
 /dev/block/platform/msm_sdcc.1/by-name/apnhlos        /firmware        vfat    ro,shortname=lower,uid=1000,gid=1026,dmask=227,fmask=337,context=u:object_r:firmware_file:s0  wait
 /dev/block/platform/msm_sdcc.1/by-name/modem          /firmware-modem  vfat    ro,shortname=lower,uid=1000,gid=1026,dmask=227,fmask=337,context=u:object_r:firmware_file:s0  wait
 
--- a/ramdisk/init.target.rc
+++ b/ramdisk/init.target.rc
@@ -52,16 +52,18 @@ on fs
     # these partition flashed on the device. Failure to mount any partition in fstab file
     # results in failure to launch late-start class.
 
-#    wait /dev/block/platform/msm_sdcc.1/by-name/cache
-#    check_fs /dev/block/platform/msm_sdcc.1/by-name/cache ext4
-#    mount ext4 /dev/block/platform/msm_sdcc.1/by-name/cache /cache nosuid nodev noatime noauto_da_alloc,discard,journal_async_commit,errors=panic
+    wait /dev/block/platform/msm_sdcc.1/by-name/cache
+    mount ext4 /dev/block/platform/msm_sdcc.1/by-name/cache /cache nosuid nodev noatime noauto_da_alloc,discard,journal_async_commit,errors=panic
+    mount f2fs /dev/block/platform/msm_sdcc.1/by-name/cache /cache nosuid nodev noatime background_gc=on,discard,user_xattr,active_logs=6
+
+    wait /dev/block/platform/msm_sdcc.1/by-name/userdata
+    mount ext4 /dev/block/platform/msm_sdcc.1/by-name/userdata /data nosuid nodev noatime noauto_da_alloc,discard,journal_async_commit,errors=panic
+    mount f2fs /dev/block/platform/msm_sdcc.1/by-name/userdata /data nosuid nodev noatime background_gc=on,discard,user_xattr,active_logs=6
 
     wait /dev/block/platform/msm_sdcc.1/by-name/persist
-    check_fs /dev/block/platform/msm_sdcc.1/by-name/persist ext4
     mount ext4 /dev/block/platform/msm_sdcc.1/by-name/persist /persist nosuid nodev noatime noauto_da_alloc,discard,journal_async_commit,errors=panic
 
     wait /dev/block/platform/msm_sdcc.1/by-name/efs
-    check_fs /dev/block/platform/msm_sdcc.1/by-name/efs ext4
     mount ext4 /dev/block/platform/msm_sdcc.1/by-name/efs /efs nosuid nodev noatime noauto_da_alloc,discard,journal_async_commit,errors=panic
      chown system radio /efs
      chmod 0771 /efs' | patch -p2

cp -p ../../ramdisk/sbin/after-boot.sh sbin/
cp -p ../../ramdisk/res/busybox res/
cp -rp ../../ramdisk/res/asset res/

touch made_by_arter97
mkdir lib
mkdir lib/modules
find . -type d -empty | while read empty_folder; do touch $empty_folder/EMPTY_DIRECTORY; done
rm -rf ../../ramdisk
mkdir ../../ramdisk
cp -rp . ../../ramdisk/

cd ../..
rm -rf tmp
rm -rf /tmp/arter97_update_ramdisk
git reset
